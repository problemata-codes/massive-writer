# Massive Writer

[Basé sur ce routeur Python](https://framagit.org/problemata-codes/routeur-python)

## Dépendances

- Flask
- pypandoc
- Pandoc
- pandocfilters
- requests
- mod_wsgi

## Installation développement

- `git clone https://framagit.org/problemata-codes/massive-writer`
- `cd massive-writer/`
- `python -m venv env/` : créer un environnement virtuel (testé avec Python 3.6.9, 3.8.10 et 3.9.1)
- `source env/bin/activate` : activer l'environnement (pour en sortir : `deactivate`)
- `pip install Flask`
- `pip install python-dotenv`
- `pip install pypandoc`
- `pip install pandocfilters`
- `pip install requests`
- `pip install beautifulsoup4`
- `pip install -e .` : installation des dépendances
- `pip show flask` : vérifier flask
- `flask --app MassiveWriter init-db` : créer la base de données
- ajouter les droits d'écriture sur `instance/content/`
- décommenter la route "Register" de `MassiveWriter/auth.py`, se rendre à l'url sur `auth/register` et créer un compte
- recommenter la route "Register" de `MassiveWriter/auth.py`
- créer `MassiveWriter/core/pandocfilters/credentials.py` avec comme contenu (en remplaçant "voir omeka s" par les vrais credentials) :

```
keyIdentity = "voir omeka s"
keyCredential = "voir omeka s"
```

- créer `MassiveWriter/static/js/credentials.js` avec comme contenu (en remplaçant "voir omeka s" par les vrais credentials) :

```
var key_identity = "voir omeka S";
var key_credential = "voir omeka S";
```

- démarrage de l'application :

```
export FLASK_APP=MassiveWriter
export FLASK_ENV=development
flask run
```

## Mise jour du code source sur la VM

- `source venv/bin/activate` : activer l'environnement (pour en sortir : `deactivate`)
- `git pull https://framagit.org/problemata-codes/massive-writer`
- `pip install -e .` : installation des dépendances si besoin

## Installation sur la VM

- `virtualenv venv` ou `virtualenv --python=/usr/bin/python3.6 venv` : créer un environnement virtuel (testé avec Python 3.9.1)
- `source venv/bin/activate` : activer l'environnement (pour en sortir : `deactivate`)
- `pip install -e .` : installation des dépendances
- dans `massive_writer.wsgi`, changer `python_home` pour pointer vers l'environnement virtuel
- ajout des droits d'écriture sur `instance/content/`
- `init-db` créer la base de données
- décommenter la route "Register" de `MassiveWriter/auth.py`, se rendre à l'url sur `auth/register` et créer un compte
- recommenter la route "Register" de `MassiveWriter/auth.py`
- créer `MassiveWriter/core/pandocfilters/credentials.py` avec comme contenu (en remplaçant "voir omeka s" par les vrais credentials) :

```
keyIdentity = "voir omeka s"
keyCredential = "voir omeka s"
```

- créer `MassiveWriter/static/js/credentials.js` avec comme contenu (en remplaçant "voir omeka s" par les vrais credentials) :

```
var key_identity = "voir omeka s";
var key_credential = "voir omeka s";
```

- paramétrer l'hôte virtuel Apache en remplaçant "massive_writer_dir" par le chemin absolu où est Massive Writer :

```
<VirtualHost *:80>
		...

        WSGIScriptAlias /massive_writer /massive_writer_dir/massive_writer.wsgi
        <Directory /massive_writer_dir>
            Order deny,allow
            Allow from all
            Require all granted
        </Directory>

        ...
</VirtualHost>
```



## Ressources

- https://flask.palletsprojects.com/en/1.1.x/tutorial/
- https://github.com/pallets/flask/tree/1.1.2/examples/tutorial
- https://flask.palletsprojects.com/en/1.1.x/patterns/distribute/#declaring-dependencies
- https://modwsgi.readthedocs.io/en/master/user-guides/virtual-environments.html
- https://stackoverflow.com/a/6591232
- https://stackoverflow.com/questions/30634777/unicodeencodeerror-when-running-in-mod-wsgi-even-after-setting-lang-and-lc-all
- https://www.tutorialspoint.com/How-to-read-and-write-unicode-UTF-8-files-in-Python

modwsgi :

- https://stackoverflow.com/questions/40391409/apache-mod-wsgi-python-doesnt-load-installed-modules
- https://docs.python.org/3/library/subprocess.html#subprocess.Popen
- http://blog.dscpl.com.au/
- https://modwsgi.readthedocs.io/en/develop/user-guides/checking-your-installation.html#python-installation-in-use
- http://eosrei.net/articles/2014/06/python-paths-subprocesspopen-apache-modwsgi-and-virtualenv
- https://stackoverflow.com/questions/65628698/subprocess-and-os-popen-in-django-not-running-under-apache-with-mod-wsgi-at-linu
- https://www.codementor.io/@abhishake/minimal-apache-configuration-for-deploying-a-flask-app-ubuntu-18-04-phu50a7ft