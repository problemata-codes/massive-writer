/*
    Script JS appelé sur le template edit
*/
function initEdit() {

    // initialisation des deux EasyMDE de la page
    initEasyMDE("textarea[name='txt-title']");
    initEasyMDE("textarea[name='txt']");
    initEasyMDE("textarea[name='fig']"); 

    // fetch omeka
    fetchOmekaByArticleID();
    fetchOmeka();
}

/*
    Initialise EasyMDE sur un textarea
*/
function initEasyMDE(selector) {
    // suppr pr l'instant les réglages autosave et forceSync car ils supprimaient le contenu ajouté
    var easyMDE = new EasyMDE({
        element: document.querySelector(selector),
        autoDownloadFontAwesome : true,
        autofocus : true,
        lineNumbers : true,
        indentWithTabs: false,
        minHeight : 1 + "rem",
        hideIcons: [],
        status: ["autosave", "lines", "words", "cursor"],
        tabSize: 4,
        uploadImage : true,
        sideBySideFullscreen: false,
        syncSideBySidePreviewScroll: false
    });
}

/*
    Fetch Omeka pour lister toutes les ressources de la base 
*/
async function fetchOmeka() {
    var api_resources_url = api_url+"?resource_class_label=Document";

    if (typeof key_identity !== 'undefined') {
        var api_resources_url = api_url+"?resource_class_label=Document&key_identity=" + key_identity + "&key_credential=" + key_credential;
    } else {
        var api_resources_url = api_url+"?resource_class_label=Document";
    }
    console.log(api_resources_url);
    var pbta_resources = await fetchAPI(api_resources_url);

    var container = document.querySelector("#omeka_resources_list");
    var ul = document.createElement("UL");
    container.appendChild(ul);

    pbta_resources.forEach(function(resource) {
        var li = document.createElement("LI");
        ul.appendChild(li);

        var div_id = document.createElement("DIV");
            div_id.classList.add("r_id");
            li.appendChild(div_id);
        var div_title = document.createElement("DIV");
            div_title.classList.add("r_title");
            li.appendChild(div_title);
        var div_img = document.createElement("DIV");
            div_img.classList.add("r_img");
            li.appendChild(div_img);

        div_id.innerHTML = "[[" + resource["o:id"] + "]]";
        div_title.innerHTML = resource["o:title"];
        if (resource["dcterms:identifier"]) div_title.innerHTML += " - " + resource["dcterms:identifier"][0]["@value"];

        // img
        if (resource["pb:media"].length) {
            var media_src = resource["pb:media"][0]["o:thumbnail_urls"]["medium"];
            var img = document.createElement("IMG");
            img.src = media_src;
            div_img.appendChild(img);
        }
        
    });
}

/*
    Fetch Omeka pr lister les ressources liés à l'article
    + génération des pseudo-codes figures
*/
async function fetchOmekaByArticleID() {

    var o_id = document.querySelector("input[name=o_id]").value;
    
    // si pas d'oid
    if (!parseInt(o_id)) return;

 
    if (typeof key_identity !== 'undefined') {
        var api_article_url = api_url+"/"+o_id+"?key_identity=" + key_identity + "&key_credential=" + key_credential;
    } else {
        var api_article_url = api_url+"/"+o_id;
    }
    
    var pbta_article = await fetchAPI(api_article_url);

    // erreur de l'API
    if ('status' in pbta_article && pbta_article.status == "error") {
        var container = document.querySelector("#article_resources_list");
        var p = document.createElement("P");
        p.innerHTML = "Problème API";
        container.appendChild(p);
    }
        
    // exception si l'item n'est pas un article ou pb API
    if (pbta_article["@type"] == undefined || pbta_article["@type"][1] != "dctype:Text") {
        var container = document.querySelector("#article_resources_list");
        var p = document.createElement("P");
        p.innerHTML = "Cet Omeka ID ne correspond pas à un Article";
        container.appendChild(p);

        var container = document.querySelector("#pseudocode_list");
        var p = document.createElement("P");
        p.innerHTML = "Cet Omeka ID ne correspond pas à un Article";
        container.appendChild(p);
    }

    /*
        https://omeka.org/s/docs/developer/api/api_reference/
        Omeka 3.0.0 permet la requête d'ids multiples
        exemple : xxx/api/item?id[]=1&id[]=2
        https://github.com/omeka/omeka-s/pull/1667
        https://github.com/omeka/omeka-s/releases/tag/v3.0.2
    */
    if ('dcterms:relation' in pbta_article) {
        var pbta_article_resources = pbta_article['dcterms:relation'];
        console.log(pbta_article_resources);
                
        var container = document.querySelector("#article_resources_list");
        var ul = document.createElement("UL");
        container.appendChild(ul);

       var pseudocode_str = "";

        pbta_article_resources.forEach(function(resource) {
            var li = document.createElement("LI");
            ul.appendChild(li);

            var div_id = document.createElement("DIV");
                div_id.classList.add("r_id");
                li.appendChild(div_id);
            var div_title = document.createElement("DIV");
                div_title.classList.add("r_title");
                li.appendChild(div_title);
            var div_img = document.createElement("DIV");
                div_img.classList.add("r_img");
                li.appendChild(div_img);

            var r_o_id = resource["@id"].split('/').pop();

            pseudocode_str += "::: {#fig?}<br><br>";
            pseudocode_str += "[["+r_o_id+"]]<br><br>"
            pseudocode_str += ":::<br><br>";
                
            div_id.innerHTML = "[[" + r_o_id + "]]";
            // console.log(resource);
            
            div_title.innerHTML = `<p>${resource['display_title']}</p>`;
            var media_name = resource["thumbnail_url"].split("/").pop();
            var media_title = resource["thumbnail_title"] ? resource["thumbnail_title"] : "";
            div_title.innerHTML += `<ul><li>${media_name}</li><li>${media_title}</li></ul>`;

            // img
            if ("thumbnail_url" in resource) {
                var media_src = resource["thumbnail_url"];
                var img = document.createElement("IMG");
                img.src = media_src;
                div_img.appendChild(img);
            }
        });

        var pseudocode_list = document.querySelector("#pseudocode_list");
        var p = document.createElement("P");
        p.innerHTML = pseudocode_str;
        pseudocode_list.appendChild(p);
    }
}

/* jdb */

/*

    - col-handler-plus && col-handler-minus grid-column-start && end resize dynamically
    - onkeydown 
        - shortcuts
            - ctrl+s && pomme+s
        - table des matières dynamiques OK
            - check cm-header in document.body.innerText || innerHTML OK

*/

window.onkeydown = function(e){
    if(e.keyCode == 13){ // # charCode 51
        //checkHash(e);
    }
}

var hashClass = null;
var hash_array = null;
function checkHash(e){
    hashClass = document.getElementsByClassName("cm-header");
    hash_array = [];
    for(var h = 0; h < hashClass.length; ++h){
        if(hashClass[h].classList.contains("cm-formatting")){}else{
            hash_array.push(
                {
                    index : h,
                    level : hashClass[h].classList[1],
                    text : hashClass[h].parentElement.innerText
                }
            );
        }
    }
    console.log(hash_array);
    appendToc(hash_array);
}

function appendToc(arr){
    var target = document.getElementById("toc-list").getElementsByTagName("ul")[0];
    console.log(target);
    target.innerHTML = "";
    for(var a = 0; a < arr.length; ++a){
        var lvl = arr[a].level.replace("cm-header-","");
        target.innerHTML += "<li class='toc-lvl-"+lvl+"'>"+arr[a].text+"</li>";
    }
}
