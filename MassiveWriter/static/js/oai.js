/*django : output url*/

var protocol = "http://";
var site_root = "problemata.org/";
var language = ""; //"fr/"
var urls = {
    django : {
        article : protocol + site_root + language +"articles/",
        resource : protocol + site_root + language +"resources/",
        line : protocol + site_root + language +"lines/",
        author : protocol + site_root + language +"authors/",
    },
    omeka : {
        article : null,
        resource : null,
        line : null
    }
};

/*omeka : api url + credentials*/

var url_django_articles = "http://problemata.org/fr/articles/";
var url_omeka_api_root = "";

//var url_omeka_api = "https://vincent-maillard.fr/projets/problemata/codes/omeka-s-3.1.2/api/items";

//prod
var url_omeka_api = "http://problemata.org/omeka_beta/api/items";
var url_omeka_api_lines = "http://problemata.org/omeka_beta/api/item_sets";
var url_omeka_admin = "http://problemata.org/omeka_beta/admin/item?";
var url_omeka_admin = "http://problemata.org/omeka_beta/admin/item/";
var url_omeka_admin_lines = "http://problemata.org/omeka_beta/admin/item-set/";

//dev = jdb
//var url_omeka_api = "http://localhost/omeka-s/api/items";
//var url_omeka_api_lines = "http://localhost/omeka-s/api/item_sets";

/*variables*/

var btns = document.getElementById("btns");
var btn_update = document.getElementById("btn-update");
var btn_select = document.getElementById("btn-select");
var btn_override = document.getElementById("btn-override");
var new_field_for_actual_resource_identifier = "dc:"+"name_the_field";

var global_data = {};
var item_count = 0;
var counters = {
    total: 0,
    matched : 0,
    populated : 0,
    tobeupdated : 0,
    queued : 0
};

var time_elapsed = 1000; //ms
var queue = [];
var iteration = 0;
var current_label = null;
var time_wait_before_reset = 5000;
var php_url = "http://localhost/all/git/www.problemata.org/oai-operator/process.php";

/*functions*/

document.addEventListener("DOMContentLoaded", function() {
    init();
}, false);

async function init() {
    var buttons = document.querySelectorAll("#buttons_get button");
    buttons.forEach(button => button.addEventListener("click", handleButtonClick));
    //ajout
    btn_update.addEventListener("click",updateItems);
    btn_select.addEventListener("click",selectItems);
    btn_override.addEventListener("click",switchOverrideMode);
}

function handleButtonClick() {
    var resource_class_label = this.getAttribute("resource_class_label");
    //ajout
    current_label = resource_class_label;
    resetList();
    fetchData(resource_class_label);
}

function resetList() {
    var container = document.querySelector("#list");
    while (container.children.length) container.removeChild(container.children[0]);
    //ajout
    enableItems();
    resetHeader();
    resetCounters();
    document.body.setAttribute("data-override","false");
}
function resetHeader(){
    //btns.style.display = "none";
    global_data = {};
    item_count = 0;
    iteration = 0;
    queue = [];
    updateQueue();
    document.getElementById("btn-select").classList.remove("unselect");
    document.getElementById("btn-select").classList.add("select");
}

function resetCounters(){
    for(var key in counters){
        counters[key] = 0;
    }
    document.getElementById("count-total").innerHTML = 0;
    document.getElementById("count-selected").innerHTML = 0;
}
function appendData(api_response) {
    var container = document.querySelector("#list");
    if (container.querySelector("ul")) {
        var ul = container.querySelector("ul");
    } else {
        var ul = document.createElement("UL");
    }

    api_response.data.forEach(function(datum) {
        
        global_data[datum["o:id"]] = datum;
        item_count += 1;

        //console.log(datum);
        var li = document.createElement("LI");

        //modif/ajout
        li.id = "item-"+datum["o:id"];
        li.classList.add("item");

        var type = null;
        if(datum["o:resource_class"]["o:id"] == 49){ type = "resource"; }
        if(datum["o:resource_class"]["o:id"] == 31){ type = "article"; }
        if(datum["o:resource_class"]["o:id"] == 23){ type = "line"; }
        if(datum["o:resource_class"]["o:id"] == 98){ type = "author"; }

        li.classList.add("item-"+type);

        var c_link = url_omeka_admin;
        if(type == "line"){
            c_link = url_omeka_admin_lines;
        }
        li.innerHTML = "<span class='overflowed' data-ispublic='"+datum["o:is_public"]+"'><a href='"+c_link+datum["o:id"]+"' target='_blank'>"+datum["o:id"]+". "+datum["o:title"]+"</a></span>";
        var wrap = document.createElement("div");
        wrap.classList.add("wrap");
        li.appendChild(wrap);

        var info = document.createElement("div");
        info.classList.add("info");
        wrap.appendChild(info);

        //console.log(datum);
        var new_identifier = urls.django[type] + datum["o:id"];
        //console.log(new_identifier);
        //author[733] has datum[o:resource_class][o:id] set on 1 instead of 98, and datum[@type]: Array [ "o:Item", "dcterms:Agent" ]

        if("dcterms:identifier" in datum){
            if(datum["dcterms:identifier"][0]["@value"] == new_identifier){
                counters.matched += 1;
            }else{
                if(type == "resource"){
                    if("bibo:identifier" in datum){
                        counters.tobeupdated += 1;
                    }else{
                        counters.populated += 1;
                    }
                }else{
                    counters.populated += 1;
                }
            }
        }else{
            counters.tobeupdated += 1;
        }
        counters.total += 1;

        // span pour afficher dc:identifier actuel ou le placeholder
        var actual = document.createElement("span");
        // placeholder si pas de contenu sur dc:identifier
        var identifier = "no dc:identifier yet";
        var has_dc = false;
        if("dcterms:identifier" in datum) {
            has_dc = datum["dcterms:identifier"][0]["@value"];
        // s'il y a déjà contenu dans dc:identifier
            identifier = datum["dcterms:identifier"][0]["@value"];
            // class pour afficher le contenu existant dans une couleur spéciale
            actual.classList.add("dc-actual-exist");
            // class pour réordonner les lignes en fonction de leur statut sur dc:identifier
            li.classList.add("populated");
        }

        if(type == "resource"){
            if("bibo:identifier" in datum){
                li.classList.add("biboed");
                console.log(datum["bibo:identifier"][0]["@value"]);
                var bibo = datum["bibo:identifier"][0]["@value"];
                console.log(bibo);
                if(has_dc != false){
                    if(bibo == has_dc){
                        li.classList.add("bibo_matched");
                    }else{
                        li.classList.add("bibo_different");
                    }
                }
            }
        }

        actual.classList.add("dc-actual");
        actual.innerHTML = identifier;
        info.appendChild(actual);

        // span pour afficher le futur dc:identifier
        var neo = document.createElement("span");
        neo.classList.add("dc-neo");
        neo.innerHTML = new_identifier;
        info.appendChild(neo);

        // checkbox
        var checkbox = document.createElement("input");
        checkbox.type = "checkbox";
        checkbox.classList.add("item-checkbox");
        checkbox.setAttribute("data-oid",datum["o:id"]);
        checkbox.setAttribute("data-type",type);
        checkbox.addEventListener("click",addItemToQueue);
        checkbox.value = "off";
        checkbox.checked = false;
        wrap.appendChild(checkbox);

        var stat = document.createElement("span");
        stat.className = "stat item-stat ready";
        wrap.appendChild(stat);
        li.appendChild(wrap);

        if(actual.innerHTML == neo.innerHTML){
            stat.className = "stat item-stat match";
            // class pour cacher le contenu existant sur dc:identifier
            li.classList.add("matched");
        }
        //end
        ul.appendChild(li);
    });
    console.log(counters);
    count_possibilities();
    container.appendChild(ul);
    // ajout
    btns.style.display = "flex";
    //console.log(Object.keys(global_data).length,"items",global_data);
}

function count_possibilities(){
    document.getElementById("count-total").innerHTML = counters.tobeupdated;
}

async function fetchData(resource_class_label) {

    // disable
    document.getElementById("status").innerHTML = "<div>Fetching, please wait (there might be several pages to load, take your time)</div>";
    changeDisabledStatus("disabled");

    var url_to_fetch = url_omeka_api + "?resource_class_label=" + resource_class_label;
    if(resource_class_label == "Line"){
        url_to_fetch = url_omeka_api_lines;
    }else{
        var api_params = { "basic": {}, "rdf": {}, "ids": [] };
        api_params.basic.sort_by = "id";
        api_params.basic.sort_order = "desc";
    }

    console.log(url_to_fetch,api_params);
    var api_response = await apiGET(url_to_fetch, api_params);
    if(api_response){
        appendData(api_response);
        if ('next' in api_response.pagination) {
            fetchNextPage(api_response, resource_class_label);
        }else{
        var this_type = "article";
        if(current_label == "Document"){ this_type = "resource"; }
        if(current_label == "Line"){ this_type = "line"; }
            document.getElementById("status").innerHTML = "<div>Fetching complete, <strong>" + counters.total + "</strong> item(s) <i>" + this_type + "</i> has been loaded. <span class='color-matched'>" + counters.matched +"</span> are up-to-date. <span class='color-populated'>" + counters.populated + "</span> can be updated with caution. <span class='color-updated'>" + counters.tobeupdated + "</span> can be updated safely.<br>Click buttons to fetch again.</div>";
            changeDisabledStatus("enabled");
        }
    }
}
function changeDisabledStatus(etat){
    if(etat == "disabled"){
        document.body.classList.remove("enabled");
        document.body.classList.add(etat);
    }else{
        document.body.classList.remove("disabled");
        document.body.classList.add(etat);
    }
}

async function fetchNextPage(api_response, resource_class_label) {
    var page_number = api_response.pagination.next.page;
    var api_params = { "basic": {}, "rdf": {}, "ids": [] };
    api_params.basic.sort_by = "id";
    api_params.basic.sort_order = "desc";
    api_params.basic.page = page_number;

    var url_to_fetch = url_omeka_api + "?resource_class_label=" + resource_class_label;
    var api_response = await apiGET(url_to_fetch, api_params);
    appendData(api_response);

    if ('next' in api_response.pagination) {
        document.getElementById("status").innerHTML = "<div>Fetching, please wait (there might be several pages to load, take your time)</div>";
        document.getElementById("status").innerHTML += "<div>Fetching page " + (api_response.pagination.next.page-1) + "/" + (api_response.pagination.last.page-1) + "...</div>";
        fetchNextPage(api_response, resource_class_label);
    }else{
        var this_type = "article";
        if(current_label == "Document"){ this_type = "resource"; }
        document.getElementById("status").innerHTML = "<div>Fetching complete, <strong>" + counters.total + "</strong> item(s) <i>" + this_type + "</i> has been loaded. <span class='color-matched'>" + counters.matched +"</span> are up-to-date. <span class='color-populated'>" + counters.populated + "</span> can be updated with caution. <span class='color-updated'>" + counters.tobeupdated + "</span> can be updated safely.<br>Click buttons to fetch again.</div>";
        changeDisabledStatus("enabled");
    }
}

async function apiGET(url, params) {

    // Access-Control-Allow-Origin
    var options = {
        method: 'GET',
        mode: "cors"
    };

    if (params) url += "&" + buildQuery(params);

    var response = await fetch(url, options);

    if (response.status !== 200) {
        console.log(response);
        return generateErrorResponse('Erreur');
    }

    var data = await response.json();
    var pagination = parsePagination(response.headers.get("Link"));
    var total = response.headers.get("Omeka-S-Total-Results");

    var result = {
        "data": data,
        "pagination": pagination,
        "total": total
    }
    
    return result;
}

async function apiPUT(url, json_payload) {
    /*
        "url" sour la forme de /api/:api_resource/:id
        "json_payload" = copie d'un item json donné par l'API Omeka
    */
    url += `?key_identity=${key_identity}&key_credential=${key_credential}`;

    var options = {
        method: 'PUT',
        credentials: 'include',
        headers: {
            'Accept' : 'application/json',
            'Content-Type': 'application/json',
            'Origin' : 'http://localhost'
        },
        mode: "cors",
        body: JSON.stringify(json_payload)
    };

    var response = await fetch(url, options);
    if (response.status !== 200) {
        return "error";
        //return generateErrorResponse('Erreur');
    }
    var data = await response.json();
    //console.log(data);
    if(data){
        return data;
    }else{
        return null;
    }
}

/* "next": { "url": "xxx", "page": "2" } */
function parsePagination(s) {
    // https://stackoverflow.com/a/54474191
    let re = /<([^>]+[a-z]+=([\d]+))>;[\s]*rel="([a-z]+)"/g;
    // let re = /<([^>]+(?:page)+=([\d]+))[^>]*>;[\s]*rel="([a-z]+)"/g;
    let arrRes = [];
    let obj = {};
    while ((arrRes = re.exec(s)) !== null) {
        obj[arrRes[3]] = {
            url: arrRes[1],
            page: arrRes[2]
        };
    }
    return obj;
}


function buildQuery(params) {
    // ajout credentials
    if (typeof key_identity !== 'undefined') {
        params.basic.key_identity = key_identity 
        params.basic.key_credential = key_credential;
    }

    // parametre rdf
    var rdf_arrays = [];
    var exceptions = ["dcterms:creator", "dcterms:relation"]
    for (var [rdf, arg_value] of Object.entries(params.rdf)) {
        var search_type = exceptions.includes(rdf) ? "res" : "in";
        if (arg_value) {
            obj = {
                'joiner': 'and',
                'property': rdf,
                'type': search_type,
                'text': arg_value
            }
            rdf_arrays.push(obj); 
        }
    }
    // transformation de la list d'obj rdf en une str requete Omeka
    var rdf_params_str = arrayToQueryString(rdf_arrays);

    // parametre basic
    var basic_params_str = "";
    for (var [rdf, arg_value] of Object.entries(params.basic)) {
        if (rdf != "page") basic_params_str += `${rdf}=${arg_value}&`;
    }
    // on assure que 'page' est tjrs a la fin (cf regex de parsePagination)
    if ("page" in params.basic) basic_params_str += `page=${params.basic.page}&`;

    // parametre ids
    var ids_str = params.ids.map(id => "id[]="+id).join('&');

    var full_str = [rdf_params_str, basic_params_str, ids_str].filter(Boolean).join('&');
    return full_str;
}

function arrayToQueryString(objects) {
    var query_array = [];
    for (var i = 0; i < objects.length; i++) {
        var obj = objects[i];
        var query = Object.keys(obj).map(key => "property["+i+"]["+key+"]=" + obj[key]).join('&');
        // property[0][joiner]=and&property[0][property]=dcterms:language&property[0][type]=in&property[0][text]=de
        query_array.push(query);
    }
    var full_query_str = query_array.join('&');
    return full_query_str;
}



/* AJOUTS FONCTIONS */
/* une fois que la liste d'items est chargée */
/* afficher un bouton update this list */
/* au clic du bouton update this list */
/* on parcourt chaque item un par un (récursivement) avec une fonction updata API-URL(type,ID,json)*/
    /* update dc:identifier(s) = getPrefixUrl(type) + oid */
/* une fois qu'on a la réponse, on l'affiche et on check is c'est good ou non (status = green || red) */
/* on avance au prochain item et ainsi de suite jusqu'à la fin*/

function updateItems(){
    //console.log(this,queue);
    if(document.body.getAttribute("data-override") == "true"){
        if (window.confirm("Are you sure ?")) {
            updateItemRecursively();
        }else{
            return false;
        }

    }else{
        updateItemRecursively();
    }
}
function selectItems(){
    var state = "on";
    if(this.classList.contains("select")){
        this.classList.remove("select");
        this.classList.add("unselect");
    }else{
        this.classList.remove("unselect");
        this.classList.add("select");
        state = "off";
    }
    //console.log("new state :",state);
    var items = document.getElementsByClassName("item");

    for(var i = 0; i < items.length; ++i){
        var checker = items[i].getElementsByTagName("input")[0];
        //console.log(items[i]);
        var over = document.body.getAttribute("data-override");
        if(over == "true"){
            checker.value = state;
            if(items[i].classList.contains("matched")){
                checker.checked = false;
                checker.value = "off";
                checker.parentElement.getElementsByClassName("stat")[0].classList.remove("queued");
                checker.parentElement.getElementsByClassName("stat")[0].classList.add("ready");
            }else{
                if(state == "on"){
                    checker.checked = true;
                    checker.parentElement.getElementsByClassName("stat")[0].classList.remove("ready");
                    checker.parentElement.getElementsByClassName("stat")[0].classList.add("queued");
                }else if(state == "off"){
                    checker.checked = false;
                    checker.parentElement.getElementsByClassName("stat")[0].classList.remove("queued");
                    checker.parentElement.getElementsByClassName("stat")[0].classList.add("ready");
                }
            }
        }else{
            if(items[i].classList.contains("matched")){
                checker.value = "off";
                checker.checked = false;
                checker.parentElement.getElementsByClassName("stat")[0].classList.remove("queued");
                checker.parentElement.getElementsByClassName("stat")[0].classList.add("ready");
            }else if(items[i].classList.contains("populated")){
                if(items[i].classList.contains("biboed")){
                    checker.value = state;
                    if(state == "on"){
                        checker.checked = true;
                        checker.parentElement.getElementsByClassName("stat")[0].classList.remove("ready");
                        checker.parentElement.getElementsByClassName("stat")[0].classList.add("queued");
                    }else if(state == "off"){
                        checker.checked = false;
                        checker.parentElement.getElementsByClassName("stat")[0].classList.remove("queued");
                        checker.parentElement.getElementsByClassName("stat")[0].classList.add("ready");
                    }
                }else{
                    checker.value = "off";
                    checker.checked = false;
                    checker.parentElement.getElementsByClassName("stat")[0].classList.remove("queued");
                    checker.parentElement.getElementsByClassName("stat")[0].classList.add("ready");
                }
            }else{
                checker.value = state;
                if(state == "on"){
                    checker.checked = true;
                    checker.parentElement.getElementsByClassName("stat")[0].classList.remove("ready");
                    checker.parentElement.getElementsByClassName("stat")[0].classList.add("queued");
                }else if(state == "off"){
                    checker.checked = false;
                    checker.parentElement.getElementsByClassName("stat")[0].classList.remove("queued");
                    checker.parentElement.getElementsByClassName("stat")[0].classList.add("ready");
                }
            }
        }
    }
    updateQueue();
}
function addItemToQueue(){
    if(this.value == "on"){
        this.value = "off";
        this.checked = false;
        this.parentElement.getElementsByClassName("stat")[0].classList.remove("queued");
        this.parentElement.getElementsByClassName("stat")[0].classList.add("ready");
    }else if(this.value == "off"){
        this.value = "on";
        this.checked = true;
        this.parentElement.getElementsByClassName("stat")[0].classList.remove("ready");
        this.parentElement.getElementsByClassName("stat")[0].classList.add("queued");
    }
    updateQueue();
}
function updateQueue(){
    queue = [];
    var items = document.getElementsByClassName("item");
    var ind = 0;
    for(var c = 0; c < items.length; ++c){
        var checkbox = items[c].getElementsByClassName("item-checkbox")[0];
        if(checkbox.value == "on"){
            //console.log(ind, items[c]);
            ind+= 1;
            queue.push([checkbox.getAttribute("data-oid"),checkbox.getAttribute("data-type")]);
        }
    }
    document.getElementById("count-selected").innerHTML = queue.length;
    //console.log(queue);
    if(queue.length > 0){
        document.getElementById("btn-update").classList.remove("selection-disabled");
    }else{
        document.getElementById("btn-update").classList.add("selection-disabled");
    }
}

/*manipulating objects*/

function updateItemRecursively(){
    //check queue.keys.length to set stop parameters
    disableItems();
    fetchAPIUpdate();
}

async function fetchAPIUpdate(){
    var keys = Object.keys(queue);
    if(keys.length > 0){
        var basic_html = "<div>Update has started, processing "+(keys.length)+" item(s). Time elapsed between 2 items is set at "+(time_elapsed/1000)+" second(s). Estimated end of job in : " + (((time_elapsed/1000)*keys.length) - iteration + 1) + " seconds.</div>";
        if(iteration == 0){
            document.getElementById("status").innerHTML = basic_html;
        }
        document.getElementById("status").innerHTML = basic_html + "<div>Updating "+(iteration+1)+"/"+(keys.length)+" item(s) ...</div>";

        var oid = queue[iteration][0];
        var type = queue[iteration][1];

        document.getElementById("item-"+oid).getElementsByClassName("stat")[0].classList.remove("ready");
        document.getElementById("item-"+oid).getElementsByClassName("stat")[0].classList.add("progress");

        // fetch this one
        var api_params = { "basic": {}, "rdf": {}, "ids": [oid] };
        var api_url = url_omeka_api;
        if(current_label == "Line"){
            //console.log('THIS IS A LINE TYPE OBJECT');
            api_url = url_omeka_api_lines;
        }
        var api_response = await apiGET(api_url+"?", api_params);
        var data = api_response.data[0];
        //console.log(data);
        //line items not passing this step (might be because of params)

        if(data){
            //console.log("oid :",oid,"data :",data);
            document.getElementById("item-"+oid).getElementsByClassName("stat")[0].classList.remove("progress");
            document.getElementById("item-"+oid).getElementsByClassName("stat")[0].classList.add("fetched");
            //document.getElementById("item-"+oid).getElementsByClassName("stat")[0].classList.add(data);

            // exemple de mini modif faite sur le json :
            var data_modified = data;
            //console.log(data_modified);
            //data_modified["dcterms:title"][0]["@value"] = "!" + data_modified["dcterms:title"][0]["@value"];

            // VM : appel fonction pour déplacer les données de dcterms:identifier à bibo:identifier
            // var data_modified = migrateIdentifier(data);
            /*if(type == "resource"){
                var data_modified = migrateIdentifier(data);
            }*/

            if("dcterms:identifier" in data_modified){
                data_modified["dcterms:identifier"] = [];
            }else{
                data_modified["dcterms:identifier"] = [];
            }
            data_modified["dcterms:identifier"].push(constructIdentifier(urls.django[type] + oid));
            //console.log("oid :", oid, "data_modified :", data_modified);

            // put de la modif à l'api
            var url_to_fetch = url_omeka_api+"/"+oid;
            if(current_label == "Line"){
                url_to_fetch = url_omeka_api_lines+"/"+oid;
            }
            if(data_modified != false){
                var api_question = await apiPUT(url_to_fetch, data_modified);
                var answer = api_question;
                console.log(answer);

                document.getElementById("item-"+oid).getElementsByClassName("stat")[0].classList.remove("fetched");

                if(answer){
                    if(answer == "error"){
                        document.getElementById("item-"+oid).getElementsByClassName("stat")[0].classList.add("error");
                    }else{
                        document.getElementById("item-"+oid).getElementsByClassName("stat")[0].classList.add("success");
                    }
                    skipToTheNextOne();
                }else{
                    document.getElementById("item-"+oid).getElementsByClassName("stat")[0].classList.add("error");
                }


            }

        } else {
            console.log("SERVER ERROR");
        }
        
    }else{
        console.log("no item(s) selected");
        document.getElementById("status").innerHTML = "<div>No item(s) selected</div>";
        enableItems();
    }
}
function constructIdentifier(new_data){
    return {
        "type" : "literal",
        "property_id" : 10,
        "property_label" : "Identifier",
        "is_public" : true,
        "@value" : new_data
    };

}

function skipToTheNextOne(){
    var keys = Object.keys(queue);
    if(iteration < keys.length-1){
        document.getElementById("status-all").classList.remove("ready");
        document.getElementById("status-all").classList.add("progress");
        iteration += 1;
        setTimeout(fetchAPIUpdate,time_elapsed);
    }else{
        console.log("EOJ");
        document.getElementById("status-all").classList.remove("progress");
        document.getElementById("status-all").classList.add("ready");
        iteration = 0;
        queue = [];

        document.getElementById("status").innerHTML = "<div>Update complete, reload list "+(time_wait_before_reset/1000)+" in second(s), please wait.</div>";
        //console.log("---");
        //console.log("RELOAD LIST in ",time_wait_before_reset," second(s)");
        //console.log("---");
        setTimeout(function(){
            resetList();
            fetchData(current_label);
        },time_wait_before_reset);
    }
}
function disableItems(){
    var items = document.getElementsByClassName("item");
    for(var i = 0; i < items.length; ++i){
        items[i].classList.add("progressed");
    }
}
function enableItems(){
    var items = document.getElementsByClassName("item");
    for(var i = 0; i < items.length; ++i){
        items[i].classList.remove("progressed");
    }
}
function switchOverrideMode(){
    if(document.body.getAttribute("data-override") == "true"){
        document.body.setAttribute("data-override","false");
    }else{
        document.body.setAttribute("data-override","true");
    }
    //console.log("new state :",state);
    var items = document.getElementsByClassName("item");

    for(var i = 0; i < items.length; ++i){
        var checker = items[i].getElementsByTagName("input")[0];
        checker.value = "off";
        checker.checked = false;
        checker.parentElement.getElementsByClassName("stat")[0].classList.remove("queued");
        checker.parentElement.getElementsByClassName("stat")[0].className = "stat item-stat ready";
    }
    /*resetHeader();
    resetCounters();*/
    queue = [];
    btn_select.className = "select";
    btn_update.className = "selection-disabled";
    /*resetHeader();
    resetCounters();*/
}