/*
    Script JS appelé sur le template create
*/
async function initCreate() {
    var main_create = document.querySelector("#create");
    if (main_create) {

        // fetch omeka
        // ?resource_class_label=Document"
        var api_articles_url = api_url+"?resource_class_label=Text";
        
        if (typeof key_identity !== 'undefined') {
            var api_articles_url = api_url+"?resource_class_label=Text&key_identity=" + key_identity + "&key_credential=" + key_credential;
        } else {
            var api_articles_url = api_url+"?resource_class_label=Text";
        }
        var pbta_articles = await fetchAPI(api_articles_url);
        // console.log(pbta_articles);
        
        var container = document.querySelector("#articles_list");
        var ul = document.createElement("UL");
        container.appendChild(ul);

        pbta_articles.forEach(function(resource) {
            var li = document.createElement("LI");
            ul.appendChild(li);

            var div_id = document.createElement("DIV");
                div_id.classList.add("r_id");
                li.appendChild(div_id);
            var div_title = document.createElement("DIV");
                div_title.classList.add("r_title");
                li.appendChild(div_title);

            div_id.innerHTML = resource["o:id"];
            div_title.innerHTML = resource["o:title"];

        });
        
    }
}