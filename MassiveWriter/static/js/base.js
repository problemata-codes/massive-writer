document.addEventListener("DOMContentLoaded", function() {
    init();
}, false);

async function init() {

    if (document.querySelector("#edit")) initEdit();
    if (document.querySelector("#create")) initCreate();
    
}

async function fetchAPI(url) {
    var data = await request(url);
    // console.log(data);
    return data;
}


async function request(url, params, method = 'GET') {
    var options = {
      method,
      mode: "cors"
    };
    var response = await fetch(url, options);
    if (response.status !== 200) {
      return generateErrorResponse('The server responded with an unexpected status.');
    }
    var json = await response.json();
    return json;
}

function generateErrorResponse(message) {
  return {
    status : 'error',
    message
  };
}