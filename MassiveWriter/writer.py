# import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.exceptions import abort
from MassiveWriter.auth import login_required
from MassiveWriter.db import get_db

bp = Blueprint('writer', __name__)


from flask import Flask, flash, redirect, url_for, request, render_template
import io
from werkzeug.utils import secure_filename
import shutil
import pypandoc
from bs4 import BeautifulSoup

# custom scripts
from . core.utils import *

# variables globales
# ALLOWED_EXTENSIONS = {'docx'}
# API_URL = 'http://problemata.huma-num.fr/base/api/items'
API_URL = 'http://problemata.huma-num.fr/omeka_beta/api/items'

app = Flask(__name__)

# maximum allowed payload to 16 megabytes
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

"""
Session cookie pr passer des messages Flash
SECRET_KEY is used by Flask and extensions to keep data safe.
It’s set to 'dev' to provide a convenient value during development,
but it should be overridden with a random value when deploying.
python -c 'import os; print(os.urandom(16))'
"""
app.config['SECRET_KEY'] = 'dev'


@bp.context_processor
def inject_global():
    """
    Ajoute un dict au contexte des templates
    """
    return dict(api_url=API_URL)



@bp.route('/')
@bp.route('/liste')
def liste():
    items = get_items()
    return render_template('liste.html', items=items)



@bp.route('/create', methods = ['POST', 'GET'])
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        operator = request.form['operator']
        o_id = request.form['o_id']

        unique_id = uniqid()

        path = os.path.join(app.instance_path, 'content', unique_id)
        os.mkdir(path)

        date = datetime.datetime.now()

        json_dict = {
            "created": date,
            "modified": date,
            "title": title,
            "operator": operator,
            "o_id": o_id,
            "id": unique_id,
            "relative_path": os.path.join('content', unique_id)
        }
        write_json(path+"/info.json", json_dict)

        # écriture fichier "txt-title.md" sans contenu
        with open(os.path.join(path, "txt-title.md"), "w") as outputfile: 
            outputfile.write("")

        # écriture fichier "fig.md" sans contenu
        with open(os.path.join(path, "fig.md"), "w") as outputfile: 
            outputfile.write("")

        # check if the post request has the file part
        if 'file' not in request.files:
            flash("La requête POST ne contient pas de partie 'file'")
            return redirect(request.url)

        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            # écriture d'un txt.md sans contenu
            flash("Item '{}' créé sans fichier source".format(title))
            with open(os.path.join(path, "txt.md"), "w") as outputfile: 
                outputfile.write("")
            return redirect(url_for('writer.detail', item_id = unique_id))

        # check extension du fichier
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(path, filename))
        else:
            flash('Format de fichier non autorisé')
            return redirect(request.url)

        # conversion en .md par pypandoc (pandoc)
        pdoc_args = [
            '--wrap=none',
        ]
        output = pypandoc.convert_file(
            os.path.join(path, filename),
            to='md',
            format='docx',
            extra_args=pdoc_args,
            outputfile=os.path.join(path, "txt.md")
        )

        flash("Item '{}' créé à partir d'un fichier source".format(title))
        return redirect(url_for('writer.detail', item_id = unique_id))
        # abort(401)
    else:
        return render_template('create.html')



@bp.route('/detail/<item_id>')
def detail(item_id):
    item = get_item(item_id)
    path = os.path.join(app.instance_path, item['relative_path'])
    
    # f = open(os.path.join(path, "txt.md"), 'r')
    # mdString = f.read()
    with io.open(os.path.join(path, "txt.md"),'r',encoding='utf8') as f:
        txtString = f.read()

    with io.open(os.path.join(path, "fig.md"),'r',encoding='utf8') as f:
        figString = f.read()

    if os.path.isfile(os.path.join(path, "txt-title.md")):
        with io.open(os.path.join(path, "txt-title.md"),'r',encoding='utf8') as f:
            txttitleString = f.read()
    else:
        txttitleString = ""

    # conversion "txt.md" en html par pypandoc (pandoc)
    template_path = os.path.join(app.root_path, 'core/pandoc_templates/', 'view_template.html5')
    pdoc_args = [
        '-s',
        '--template={}'.format(template_path),
        '--toc',
        '--lua-filter={}'.format(os.path.join(app.root_path, 'core/pandocfilters/', 'french_typo.lua')),
        '--section-divs'
    ]
    filters = []
    txt = pypandoc.convert_text(
        txtString,
        to='html5',
        format='md',
        extra_args=pdoc_args,
        filters=filters,
    )
    item['txt'] = txt

    # conversion "txt-title.md" en html par pypandoc (pandoc)
    pdoc_args = [
        '--lua-filter={}'.format(os.path.join(app.root_path, 'core/pandocfilters/', 'french_typo.lua')),
    ]
    filters = []
    txttitle = pypandoc.convert_text(
        txttitleString,
        to='html5',
        format='md',
        extra_args=pdoc_args,
        filters=filters,
    )
    item['txttitle'] = txttitle

    # conversion "fig.md" en html par pypandoc (pandoc) avec un filtre
    pdoc_args = [
        '--lua-filter={}'.format(os.path.join(app.root_path, 'core/pandocfilters/', 'french_typo.lua')),
        '--metadata=lang:fr'
    ]
    filters = [os.path.join(app.root_path, 'core/pandocfilters/', 'o_noticemedia.py')]
    fig = pypandoc.convert_text(
        figString,
        to='html5',
        format='md',
        extra_args=pdoc_args,
        filters=filters,
    )
    item['fig'] = fig



    return render_template('detail.html', item = item)



@bp.route('/edit/<item_id>', methods = ['GET', 'POST'])
@login_required
def edit(item_id):
    item = get_item(item_id)
    path = os.path.join(app.instance_path, item['relative_path'])
    if request.method == 'POST':
        
        # modif de du dict de l'item        
        item['title'] = request.form['title']
        item['operator'] = request.form['operator']
        item['o_id'] = request.form['o_id']
        item['modified'] = datetime.datetime.now()
        # temporaire
        rel_path = path.replace(app.instance_path + "/", '')
        item['relative_path'] = rel_path

        write_json(path+"/info.json", item)

        # écriture du fichier .md
        txtString = request.form['txt']
        txttitleString = request.form['txt-title']
        figString = request.form['fig']
        #f = open(os.path.join(path, "txt.md"), 'w')
        #f.write(mdString)
        #f.close()
        with io.open(os.path.join(path, "txt.md"), 'w', encoding='utf8') as f:
            f.write(txtString)

        with io.open(os.path.join(path, "txt-title.md"), 'w', encoding='utf8') as f:
            f.write(txttitleString)

        with io.open(os.path.join(path, "fig.md"), 'w', encoding='utf8') as f:
            f.write(figString)

        # message au template
        flash("Modifications enregistrées")

    # lecture du fichier .md et ajout au dict item
    # f = open(os.path.join(path, "txt.md"), 'r')
    with io.open(os.path.join(path, "txt.md"),'r',encoding='utf8') as f:
        txtString = f.read()
    item['txt'] = txtString

    if os.path.isfile(os.path.join(path, "txt-title.md")):
        with io.open(os.path.join(path, "txt-title.md"),'r',encoding='utf8') as f:
            txttitleString = f.read()
        item['txttitle'] = txttitleString
    else:
        item['txttitle'] = ""

    # lecture du fichier figures.md et ajout au dict item
    with io.open(os.path.join(path, "fig.md"),'r',encoding='utf8') as f:
        figString = f.read()
    item['fig'] = figString

    return render_template('edit.html', item = item)



@bp.route('/delete/<item_id>', methods = ['GET', 'POST'])
@login_required
def delete(item_id):
    """
    Route pour supprimer un item
    """
    if request.method == 'POST':
        path = os.path.join(app.instance_path, 'content', item_id)
        shutil.rmtree(path)
        flash("Item '{}' supprimé".format(item_id))
        return redirect(url_for('writer.liste'))

    return render_template('delete.html', id=item_id)




@bp.route('/oai')
@login_required
def oai():
    """
    Route pour OAI
    """
    return render_template('oai.html')