
-- https://pandoc.org/lua-filters.html
-- pandoc --lua-filter=smallcaps.lua test.md -o output.html

post_glyphes = {
  "?",
  "!",
  ";",
  "»",
  "›",
  ":"
}

pre_glyphes = {
  "«",
  "‹"
}



local function has_value (tab, val)
  for index, value in ipairs(tab) do
      if value == val then
          return true
      end
  end
  return false
end



local function is_space_before_punc(spc, punc)
  return spc and spc.t == 'Space'
    and punc and has_value(post_glyphes, punc.text)
end



local function is_space_after_punc(punc, spc)
  return spc and spc.t == 'Space'
    and punc and has_value(pre_glyphes, punc.text)
end



function Inlines (inlines)
  -- Go from end to start to avoid problems with shifting indices.
  for i = #inlines-1, 1, -1 do
    if is_space_before_punc(inlines[i], inlines[i+1]) then
      local test = pandoc.Str " "
      inlines:remove(i)
      inlines:insert(i, test)
    end
    if is_space_after_punc(inlines[i], inlines[i+1]) then
      local test = pandoc.Str " "
      inlines:remove(i+1)
      inlines:insert(i+1, test)
    end
  end
  return inlines
end