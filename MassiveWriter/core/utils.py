from flask import Flask
import os
import json
import datetime
import uuid

app = Flask("MassiveWriter")
# variables globales
ALLOWED_EXTENSIONS = {'docx'}

def write_json(filepath, json_dict):
    """
    Écrit "json_dict" dans un fichier json vers "filepath"
    """
    json_object = json.dumps(json_dict, indent=4, sort_keys=True, default=str)
    with open(filepath, "w") as outfile: 
        outfile.write(json_object)



def get_item(item_id):
    """
    Retourne un dict
    """
    path = os.path.join(app.instance_path, 'content', item_id, 'info.json')
    with open(path, 'r') as openfile: 
        json_object = json.load(openfile)

        # conversion des string dates en objets datetime 
        json_object['created'] = str_to_datetime(json_object["created"])
        json_object['modified'] = str_to_datetime(json_object["modified"])

    return json_object



def get_items():
    """
    - Construit un tableau de tous les items présents dans content/
    - Retourne un tableau de dict trié par ordre croissant de date
    """
    path = os.path.join(app.instance_path, 'content/')
    items = []
    for el in os.scandir(path):
        if el.is_dir():
            item = get_item(el.name)
            items.append(item)
    
    # tri par ordre croissant de date
    items.sort(key=lambda item: item["created"])
    
    return items



def uniqid():
    number = uuid.uuid4().int
    alphabet = list("23456789ABCDEFGHJKLMNPQRSTUVWXYZ" "abcdefghijkmnopqrstuvwxyz")
    output = ""
    alpha_len = len(alphabet)
    while number:
        number, digit = divmod(number, alpha_len)
        output += alphabet[digit]
    return output[0:15]



def str_to_datetime(str_date):
    """
    Transforme une str en un objet datetime
    """
    return datetime.datetime.strptime(str_date, "%Y-%m-%d %H:%M:%S.%f")



def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
